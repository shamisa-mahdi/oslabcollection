# !/bin/bash 

echo Address: 
read input

echo "" > Q5Edited.js
echo "" > Q5Edited.java

if [ ${input: -3} == ".js" ] || [ ${input: -5} == ".java" ]
then
	while IFS= read -r line
	do
		if [ ${input: -3} == ".js" ] && ! [[ $line == *"console.log"* ]]
		then
			echo $line >> Q5Edited.js
		elif [ ${input: -5} == ".java" ] && ! [[ $line == *"system.out.print"* ]] # also println
		then
			echo $line >> Q5Edited.java
		fi

	done < "$input"
else 
	echo This file type is not supported
fi

# testFiles/Q5.js
# testFiles/Q5.java