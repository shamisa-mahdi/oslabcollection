# 1
cd /bin/
ls c* | egrep -v '^d' && ls c* | egrep -v '^d' | wc -l

# 2
cd /etc/
find . -maxdepth 1 -not -type d | head -7

# 3 cd /etc/
ls | grep z

# 4
history 5

# 5
cd /etc/
(ls | head -7 && ls | tail -6) > ~/Desktop/Q5.txt 

# 6
mkdir directory1
touch file
cd directory1/
echo "hello world!" > file
cd ..
mkdir directory2
cp directory1/file directory2
rm -rf directory1/file 
cd directory1
ls 
cd ..
cd directory2
ls
cat file

# 7
touch error.txt
ls aaaa |& tee error.txt 

# 8
cd /bin
ls | grep '\.txt$' 

# 9
     cat : -A, --show-all
              equivalent to -vET

       -n, --number
              number all output lines

       -s, --squeeze-blank
              suppress repeated empty output lines

       -e     equivalent to -vE

       -E, --show-ends
              display $ at end of each line
more :       -d     Prompt  with  "[Press  space to continue, 'q' to quit.]", and display "[Press 'h' for instructions.]" instead of ringing the
              bell when an illegal key is pressed.

       -l     Do not pause after any line containing a ^L (form feed).

       -f     Count logical lines, rather than screen lines (i.e., long lines are not folded).

       -p     Do not scroll.  Instead, clear the whole screen and then display the text.  Notice that this option is switched on automati‐
              cally if the executable is named page.

       -c     Do not scroll.  Instead, paint each screen from the top, clearing the remainder of each line as it is displayed.

       -s     Squeeze multiple blank lines into one.
less:DESCRIPTION
       Less  is  a  program  similar  to more (1), but it has many more features.  Less does not have to read the entire input file before
       starting, so with large input files it starts up faster than text editors like vi (1).  Less uses termcap (or terminfo on some sys‐
       tems),  so  it  can run on a variety of terminals.  There is even limited support for hardcopy terminals.  (On a hardcopy terminal,
       lines which should be printed at the top of the screen are prefixed with a caret.)

       Commands are based on both more and vi.  Commands may be preceded by a decimal number, called N in  the  descriptions  below.   The
       number is used by some commands, as indicated.

head: Print  the  first  10  lines  of each FILE to standard output.  With more than one FILE, precede each with a header giving the file
       name.

       With no FILE, or when FILE is -, read standard input.

       Mandatory arguments to long options are mandatory for short options too.

       -c, --bytes=[-]NUM
              print the first NUM bytes of each file; with the leading '-', print all but the last NUM bytes of each file

       -n, --lines=[-]NUM
              print the first NUM lines instead of the first 10; with the leading '-', print all but the last NUM lines of each file

       -q, --quiet, --silent
              never print headers giving file names

       -v, --verbose
              always print headers giving file names

       -z, --zero-terminated
              line delimiter is NUL, not newline

tail:  Print  the  first  10  lines  of each FILE to standard output.  With more than one FILE, precede each with a header giving the file
       name.

       With no FILE, or when FILE is -, read standard input.

       Mandatory arguments to long options are mandatory for short options too.

       -c, --bytes=[-]NUM
              print the first NUM bytes of each file; with the leading '-', print all but the last NUM bytes of each file

       -n, --lines=[-]NUM
              print the first NUM lines instead of the first 10; with the leading '-', print all but the last NUM lines of each file

       -q, --quiet, --silent
              never print headers giving file names

       -v, --verbose
              always print headers giving file names

       -z, --zero-terminated
              line delimiter is NUL, not newline
